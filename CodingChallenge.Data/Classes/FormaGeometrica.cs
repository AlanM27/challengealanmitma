﻿/*
 * Refactorear la clase para respetar principios de programación orientada a objetos. Qué pasa si debemos soportar un nuevo idioma para los reportes, o
 * agregar más formas geométricas?
 *
 * Se puede hacer cualquier cambio que se crea necesario tanto en el código como en los tests. La única condición es que los tests pasen OK.
 *
 * TODO: Implementar Trapecio/Rectangulo, agregar otro idioma a reporting.
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CodingChallenge.Data.Classes.Interfaces;
using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes
{
    public abstract class FormaGeometrica
    {
        private readonly decimal _side;
        
        private readonly TypeFormsEnum _typeForms;

        public abstract decimal CalculateArea();

        public abstract decimal CalculatePerimeter();

        public FormaGeometrica(decimal side, TypeFormsEnum typeForms)
        {
            this._typeForms = typeForms;
            this._side = side;
        }

        public static string Imprimir(List<FormaGeometrica> forms, ITranslate language)
        {
            var sb = new StringBuilder();
            var areaFigure = 0m;
            var perimeterFigure = 0m;
            var totalFigureArea = 0m;
            var totalFigurePerimeter = 0m;
            int totalFigure = 0;
            int figuresNumber = 0;

            var footer = language.GetFooter();


            if (!forms.Any())
            {
                sb.Append("<h1>" + language.GetMessage() + "</h1>");

            }
            else
            {
                sb.Append("<h1>" + language.GetHeader() + "</h1>");

                foreach (TypeFormsEnum item in (TypeFormsEnum[])Enum.GetValues(typeof(TypeFormsEnum)))
                {
                    totalFigure = forms.Where(a => a._typeForms == item).ToList().Count;
                    areaFigure = forms.Where(a => a._typeForms == item).ToList().Sum(a => a.CalculateArea());
                    perimeterFigure = forms.Where(a => a._typeForms == item).ToList().Sum(a => a.CalculatePerimeter());
                    sb.Append(language.GetLine(totalFigure, areaFigure, perimeterFigure, item));


                    figuresNumber = figuresNumber + totalFigure;
                    totalFigureArea = totalFigureArea + areaFigure;
                    totalFigurePerimeter = totalFigurePerimeter + perimeterFigure;
                }

                // FOOTER
                sb.Append(footer.Total + ":<br/>");
                sb.Append(figuresNumber + " " + footer.Form + " ");
                sb.Append(footer.Perimeter + " " + (totalFigurePerimeter).ToString("#.##") + " ");
                sb.Append(footer.Area + " " + (totalFigureArea).ToString("#.##"));
            }

            return sb.ToString();
        }

    }
}
