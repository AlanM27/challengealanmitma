﻿using CodingChallenge.Data.Classes.Interfaces;
using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes.Translation
{
    public class English : ITranslate
    {
        public string GetMessage()
        {
            var message = "Empty list of shapes!";

            return message;
        }

        public string GetHeader()
        {
            var message = "Shapes report";

            return message;
        }

        public FooterModel GetFooter()
        {
            var footerResult = new FooterModel();

            footerResult.Total = "TOTAL";
            footerResult.Area = "Area";
            footerResult.Perimeter = "Perimeter";
            footerResult.Form = "shapes";

            return footerResult;
        }

        public string GetLine(int amount, decimal area, decimal perimeter, TypeFormsEnum type)
        {
            if (amount > 0)
            {

                return $"{amount} {TranslateForm(type, amount)} | {"Area"} {area:#.##} | {"Perimeter"} {perimeter:#.##} <br/>";

            }

            return string.Empty;
        }

        public string TranslateForm(TypeFormsEnum type, int amount)
        {
            switch (type)
            {
                case TypeFormsEnum.Square:
                    return amount == 1 ? "Square" : "Squares";
                case TypeFormsEnum.Circle:
                    return amount == 1 ? "Circle" : "Circles";
                case TypeFormsEnum.EquilateralTriangle:
                    return amount == 1 ? "Triangle" : "Triangles";
                case TypeFormsEnum.Trapeze:
                    return amount == 1 ? "Trapeze" : "Trapezoids";
                case TypeFormsEnum.Rectangle:
                    return amount == 1 ? "Rectangle" : "Rectangles";
            }

            return string.Empty;
        }

    }
}
