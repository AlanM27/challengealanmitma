﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingChallenge.Data.Classes.Models
{
    public enum TypeFormsEnum
    {
        Square = 1,
        Circle = 2,
        EquilateralTriangle = 3,
        Trapeze = 4,
        Rectangle = 5
    }
}
