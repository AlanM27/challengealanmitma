﻿using System;
using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes.Forms
{
    public class Rectangle : FormaGeometrica
    {

        private readonly decimal _sideB;
        private readonly decimal _sideA;

        public Rectangle(decimal side, decimal sideA) : base(side, TypeFormsEnum.Rectangle)
        {
            _sideB = side;
            _sideA = sideA;
        }

        public override decimal CalculateArea()
        {
            return _sideB * _sideA;
        }

        public override decimal CalculatePerimeter()
        {
            return _sideB + _sideB + _sideA + _sideA;
        }

    }
}
