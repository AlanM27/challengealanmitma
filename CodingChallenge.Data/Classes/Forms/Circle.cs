﻿using System;
using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes.Forms
{
    public class Circle : FormaGeometrica
    {
        private readonly decimal _side;

        public Circle(decimal side) : base(side, TypeFormsEnum.Circle)
        {
            this._side = side;
        }

        public override decimal CalculateArea()
        {
            return (decimal)Math.PI * (_side / 2) * (_side / 2);
        }

        public override decimal CalculatePerimeter()
        {
            return (decimal)Math.PI * _side;
        }
    }
}
