﻿using System;
using System.Collections.Generic;
using CodingChallenge.Data.Classes;
using NUnit.Framework;
using CodingChallenge.Data.Classes.Translation;
using CodingChallenge.Data.Classes.Forms;

namespace CodingChallenge.Data.Tests
{
    [TestFixture]
    public class DataTests
    {
        [TestCase]
        public void TestResumenListaVacia()
        {
            Assert.AreEqual("<h1>Lista vacía de formas!</h1>",
                FormaGeometrica.Imprimir(new List<FormaGeometrica>(), new Spanish()));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnIngles()
        {
            Assert.AreEqual("<h1>Empty list of shapes!</h1>",
                FormaGeometrica.Imprimir(new List<FormaGeometrica>(), new English()));
        }

        [TestCase]
        public void TestResumenListaVaciaFormasEnFrances()
        {
            Assert.AreEqual("<h1>Liste vide de formes!</h1>",
                FormaGeometrica.Imprimir(new List<FormaGeometrica>(), new French()));
        }

        [TestCase]
        public void TestResumenListaConUnCuadrado()
        {
            var cuadrados = new List<FormaGeometrica> { new Square(5) };

            var resumen = FormaGeometrica.Imprimir(cuadrados, new Spanish());

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Cuadrado | Area 25 | Perímetro 20 <br/>TOTAL:<br/>1 Formas Perímetro 20 Area 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasCuadrados()
        {
            var cuadrados = new List<FormaGeometrica>
            {
                new Square(5),
                new Square(1),
                new Square(3)
            };

            var resumen = FormaGeometrica.Imprimir(cuadrados, new English());

            Assert.AreEqual("<h1>Shapes report</h1>3 Squares | Area 35 | Perimeter 36 <br/>TOTAL:<br/>3 shapes Perimeter 36 Area 35", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTipos()
        {
            var formas = new List<FormaGeometrica>
            {
                new Square(5),
                new Circle(3),
                new EquilateralTriangle(4),
                new Square(2),
                new EquilateralTriangle(9),
                new Circle(2.75m),
                new EquilateralTriangle(4.2m),
                new Trapeze(8, 6, 1),
                new Trapeze(9, 4, 6)
            };

            var resumen = FormaGeometrica.Imprimir(formas, new English());

            Assert.AreEqual(
                "<h1>Shapes report</h1>2 Squares | Area 29 | Perimeter 28 <br/>2 Circles | Area 13,01 | Perimeter 18,06 <br/>3 Triangles | Area 49,64 | Perimeter 51,6 <br/>2 Trapezoids | Area 46 | Perimeter 41 <br/>TOTAL:<br/>9 shapes Perimeter 138,66 Area 137,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnCastellano()
        {
            var formas = new List<FormaGeometrica>
            {
                new Square(5),
                new Circle(3),
                new EquilateralTriangle(4),
                new Square(2),
                new EquilateralTriangle(9),
                new Circle(2.75m),
                new EquilateralTriangle(4.2m),
                new Trapeze(8, 6, 1),
                new Trapeze(9, 4, 6)
            };

            var resumen = FormaGeometrica.Imprimir(formas, new Spanish());

            Assert.AreEqual(
                "<h1>Reporte de Formas</h1>2 Cuadrados | Area 29 | Perímetro 28 <br/>2 Círculos | Area 13,01 | Perímetro 18,06 <br/>3 Triángulos | Area 49,64 | Perímetro 51,6 <br/>2 Trapecios | Area 46 | Perímetro 41 <br/>TOTAL:<br/>9 Formas Perímetro 138,66 Area 137,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConMasTiposEnFrances()
        {
            var formas = new List<FormaGeometrica>
            {
                new Square(5),
                new Circle(3),
                new EquilateralTriangle(4),
                new Square(2),
                new EquilateralTriangle(9),
                new Circle(2.75m),
                new EquilateralTriangle(4.2m),
                new Trapeze(8, 6, 1),
                new Trapeze(9, 4, 6)
            };

            var resumen = FormaGeometrica.Imprimir(formas, new French());

            Assert.AreEqual(
                "<h1>Rapport de formulaires</h1>2 Carrés | Zone 29 | Périmètre 28 <br/>2 Cercles | Zone 13,01 | Périmètre 18,06 <br/>3 Triangles | Zone 49,64 | Périmètre 51,6 <br/>2 Trapèzes | Zone 46 | Périmètre 41 <br/>Le total:<br/>9 Formes Périmètre 138,66 Zone 137,65",
                resumen);
        }

        [TestCase]
        public void TestResumenListaConUnCuadradoEnFrances()
        {
            var cuadrados = new List<FormaGeometrica> { new Square(5) };

            var resumen = FormaGeometrica.Imprimir(cuadrados, new French());

            Assert.AreEqual("<h1>Rapport de formulaires</h1>1 Carré | Zone 25 | Périmètre 20 <br/>Le total:<br/>1 Formes Périmètre 20 Zone 25", resumen);
        }

        [TestCase]
        public void TestResumenListaConUnRectangulo()
        {
            var rectangulo = new List<FormaGeometrica> { new Rectangle(5,3) };

            var resumen = FormaGeometrica.Imprimir(rectangulo, new Spanish());

            Assert.AreEqual("<h1>Reporte de Formas</h1>1 Rectangulo | Area 15 | Perímetro 16 <br/>TOTAL:<br/>1 Formas Perímetro 16 Area 15", resumen);
        }

        [TestCase]
        public void TestResumenListaConMasRectangulos()
        {
            var rectangulos = new List<FormaGeometrica>
            {
                new Rectangle(4,1),
                new Rectangle(6,2),
                new Rectangle(5,3)
            };

            var resumen = FormaGeometrica.Imprimir(rectangulos, new English());

            Assert.AreEqual("<h1>Shapes report</h1>3 Rectangles | Area 31 | Perimeter 42 <br/>TOTAL:<br/>3 shapes Perimeter 42 Area 31", resumen);
        }

        [TestCase]
        public void TestResumenListaConUnRectanguloEnFrances()
        {
            var rectangulo = new List<FormaGeometrica> { new Rectangle(6,2) };

            var resumen = FormaGeometrica.Imprimir(rectangulo, new French());

            Assert.AreEqual("<h1>Rapport de formulaires</h1>1 Rectangle | Zone 12 | Périmètre 16 <br/>Le total:<br/>1 Formes Périmètre 16 Zone 12", resumen);
        }

    }
}
