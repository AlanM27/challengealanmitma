﻿using CodingChallenge.Data.Classes.Interfaces;
using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes.Translation
{
    public class Spanish : ITranslate
    {
        public string GetMessage()
        {
            var message = "Lista vacía de formas!";

            return message;
        }

        public string GetHeader()
        {
            var message = "Reporte de Formas";

            return message;
        }

        public FooterModel GetFooter()
        {
            var footerResult = new FooterModel();

            footerResult.Total = "TOTAL";
            footerResult.Area = "Area";
            footerResult.Perimeter = "Perímetro";
            footerResult.Form = "Formas";

            return footerResult;
        }

        public string GetLine(int amount, decimal area, decimal perimeter, TypeFormsEnum type)
        {
            if (amount > 0)
            {

                return $"{amount} {TranslateForm(type, amount)} | {"Area"} {area:#.##} | {"Perímetro"} {perimeter:#.##} <br/>";

            }

            return string.Empty;
        }

        public string TranslateForm(TypeFormsEnum type, int amount)
        {
            switch (type)
            {
                case TypeFormsEnum.Square:
                    return amount == 1 ? "Cuadrado" : "Cuadrados";
                case TypeFormsEnum.Circle:
                    return amount == 1 ? "Círculo" : "Círculos";
                case TypeFormsEnum.EquilateralTriangle:
                    return amount == 1 ? "Triángulo" : "Triángulos";
                case TypeFormsEnum.Trapeze:
                    return amount == 1 ? "Trapecio" : "Trapecios";
                case TypeFormsEnum.Rectangle:
                    return amount == 1 ? "Rectangulo" : "Rectangulos";
            }

            return string.Empty;
        }
    }
}
