﻿using CodingChallenge.Data.Classes.Interfaces;
using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes.Translation
{
    public class French : ITranslate
    {

        public string GetMessage()
        {
            var message = "Liste vide de formes!";

            return message;
        }

        public string GetHeader()
        {
            var message = "Rapport de formulaires";

            return message;
        }

        public FooterModel GetFooter()
        {
            var footerResult = new FooterModel();

            footerResult.Total = "Le total";
            footerResult.Area = "Zone";
            footerResult.Perimeter = "Périmètre";
            footerResult.Form = "Formes";

            return footerResult;
        }

        public string GetLine(int amount, decimal area, decimal perimeter, TypeFormsEnum type)
        {
            if (amount > 0)
            {

                return $"{amount} {TranslateForm(type, amount)} | {"Zone"} {area:#.##} | {"Périmètre"} {perimeter:#.##} <br/>";

            }

            return string.Empty;
        }

        public string TranslateForm(TypeFormsEnum type, int amount)
        {
            switch (type)
            {
                case TypeFormsEnum.Square:
                    return amount == 1 ? "Carré" : "Carrés";
                case TypeFormsEnum.Circle:
                    return amount == 1 ? "Cercle" : "Cercles";
                case TypeFormsEnum.EquilateralTriangle:
                    return amount == 1 ? "Triangle" : "Triangles";
                case TypeFormsEnum.Trapeze:
                    return amount == 1 ? "Trapèze" : "Trapèzes";
                case TypeFormsEnum.Rectangle:
                    return amount == 1 ? "Rectangle" : "Rectangles";
            }

            return string.Empty;
        }

    }
}
