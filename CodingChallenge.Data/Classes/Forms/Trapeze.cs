﻿using CodingChallenge.Data.Classes.Models;

namespace CodingChallenge.Data.Classes.Forms
{
    public class Trapeze : FormaGeometrica
    {
        private readonly decimal _minorBase;
        private readonly decimal _majorBase;
        private readonly decimal _height;

        public Trapeze(decimal side, decimal majorBase, decimal height) : base(side, TypeFormsEnum.Trapeze)
        {
            this._minorBase = side;
            this._majorBase = majorBase;
            this._height = height;
        }

        public override decimal CalculateArea()
        {
            return ((_minorBase + _majorBase) / 2) * _height;
        }

        public override decimal CalculatePerimeter()
        {
            return (2 * _height) + (_minorBase + _majorBase);
        }
    }
}
