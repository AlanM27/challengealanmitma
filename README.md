# ChallengeAlanMitma

Challenge Técnico de Alan Mitma.

**Implementacion**

Luego de descargar el proyecto.

1_ Abrir la solucion.

2_ Desde el Administrador de paquetes NuGet desinstalar :
    - MSTest.TestAdapter
    - MSTest.TestFramework 
    - NUnit
    - NUnit.ConsoleRunner
    - NUnit3TestAdapter

3_ Desde el Nuget volver a instalar la ultima version de los paquetes mencionados en el punto anterior.

4_ De esta forma se soluciona los errores de referencias en el proyecto CodingChallenge.Data.Tests.

Fin Implementación.

Saludos!!
